import React, { Component } from 'react';
import {
  StyleSheet,
  Navigator,
  Text,
  View,
  BackAndroid,
  Platform,
  Modal,
  TouchableHighlight,
  NativeModules,
  Linking,
  NativeAppEventEmitter,
  DeviceEventEmitter
} from 'react-native';
import {connect, Provider} from 'react-redux';
import {getRouteMap, registerNavigator, getNavigator} from './Route';
import configureStore from './store/configure-store';

const store = configureStore();

export default class App extends Component {
  constructor(props) {
    super(props);
    this.configureScene = this.configureScene.bind(this);
    this.renderScene = this.renderScene.bind(this);
    this.onBackAndroid = this.onBackAndroid.bind(this);
  }

  componentDidMount()
  {
    //Register Back Button on fuckin Android phone
    if (Platform.OS === 'android') {
        BackAndroid.addEventListener('hardwareBackPress', this.onBackAndroid);
    }
  }

  render() 
  {
    return (
      <View style={styles.container}>
         <Provider store={store}>
            <Navigator
                ref={component => this._navigator = component}
                sceneStyle={styles.navigator}
                configureScene={this.configureScene}
                renderScene={this.renderScene}
                initialRoute={{
                    name: "SplashPage"
                }}
            />
        </Provider>
      </View>
    );
  }

  configureScene(route) 
  {
    let sceneAnimation = getRouteMap().get(route.name).sceneAnimation;
    if (sceneAnimation) {
        return sceneAnimation;
    }
    
    return Navigator.SceneConfigs.FloatFromRight;
  }

  renderScene(route, navigator) 
  {
      this.navigator = navigator;
      registerNavigator(navigator);
      
      let Component = getRouteMap().get(route.name).component;
      if (!Component) {
          return (
              <View style={styles.errorView}>
                  <Text style={styles.errorText}>Component Aren't Registered in Route Map, Go Fuck YourSelf</Text>
              </View>
          );
      }
      return (
          <Component {...route}/>
      );
  }

  onBackAndroid() {
        if (!navigator) return false;

        const routers = this.navigator.getCurrentRoutes();
        if (routers.length > 1) {
            this.navigator.pop();
            return true;
        }
        let now = new Date().getTime();
        if (now - lastClickTime < 2500) {//2.5秒内点击后退键两次推出应用程序
            // return false;//控制权交给原生
            NativeCommonTools.onBackPressed();
            return false;
        }
        lastClickTime = now;
        console.log('Press again to exit');
        return true;
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navigator: {
      flex: 1,
      backgroundColor: 'white'
  },
  errorView: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white'
  },
  errorText: {
      color: 'red',
      fontSize: 16
  }
});
