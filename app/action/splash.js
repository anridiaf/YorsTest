'use strict'
export const GETHOME_SUCCESS = 'GETHOME_SUCCESS';
export const GETHOME_FAILURE = 'GETHOME_FAILURE';
import {fetchHelper} from '../Fetch';


export const home = ()=>{
    return (dispatch)=>{
        return fetchHelper('/phoneNumber','GET')
            .then(result=>{
                return dispatch(getHomeSuccess(result));
            })
            .catch(error=>{
                return dispatch(getHomeFailure(error));
            });
    }
}

const getHomeSuccess=(result)=>({type:GETHOME_SUCCESS,result,loading:false});
const getHomeFailure=(error)=>({type:GETHOME_FAILURE,error,loading:false});
