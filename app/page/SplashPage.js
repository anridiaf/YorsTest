import React from 'react';
import{
    View,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import {getNavigator} from '../Route';
import BasePage from './BasePage';
import commonStyle from '../style/commonStyle';
import {home} from '../action/splash';

class SplashPage extends BasePage {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentWillMount() {
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    async componentDidMount() {
        const{dispatch} = this.props;
        this.setLoadingVisible(true);
        dispatch(home()).then(result => {
            if (result) {
                this.setLoadingVisible(false);
            }
        });
    }
    renderBody() {
        return (
            <View style={styles.launchImageStyle}>
                <Text style={styles.splashText1}>{"Yors Contact App"}</Text>
            </View>
        );
    }

    renderHeader()
    {

    }

    renderFooter()
    {

    }
}
const styles = StyleSheet.create({
    launchImageStyle: {
        flex: 1,
        width:commonStyle.windowWidth,
        height:commonStyle.windowHeight,
		backgroundColor: 'white',
        alignItems: 'center',
        justifyContent:'center'
    },
    splashText1: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 30,
    },
    Btn:{
        width:commonStyle.windowWidth/2, 
        height: 50, 
        alignItems:'center', 
        justifyContent:'center',
        borderRadius:5
    },
    fontBtn:{
        color:'white', 
        fontWeight:'bold', 
        fontSize:16
    }
})

function mapStateToProps(state) {
    const {home} = state;
    return {
        home
    }
}

export default connect(mapStateToProps)(SplashPage);